# spark-submit --master ${SPARK_MASTER_URL}   --executor-cores 1 --executor-memory 2G   --total-executor-cores 47 ./twitter-example.py

import sys
from pyspark.sql import SparkSession
import time

spark = SparkSession.builder \
          .appName("twitter-example") \
          .getOrCreate()

print("############################################################################")
print(" Read data from bz2 file.")
print("############################################################################")
decahosePartition = "/scratch/support_root/support/jonpot/decahose.2020-12-25.p1.bz2"
startTime = int(time.time())
df1 = spark.read.json(decahosePartition)
endTime = int(time.time())
readBz2Time = endTime - startTime

print("############################################################################")
print(" Write data to parquet file.")
print("############################################################################")
outFile = "/scratch/support_root/support/jonpot/decahose.2020-12-25.p1.parquet"
startTime = int(time.time())
df1.write.parquet(outFile)
endTime = int(time.time())
writeParquetTime = endTime - startTime

print("############################################################################")
print(" Read data from parquet file.")
print("############################################################################")
decahosePartition = "/scratch/support_root/support/jonpot/decahose.2020-12-25.p1.parquet"
startTime = int(time.time())
df2 = spark.read.parquet(decahosePartition)
endTime = int(time.time())
readParquetTime = endTime - startTime

# Display the columns.
#df2.columns
# Display the first tweet.
#df2.first()

# Group tweets by possibly sensitive and count the tweets in each group. Takes about 2 minutes.
print("############################################################################")
print(" Run groupBy() aggregation.")
print("############################################################################")
startTime = int(time.time())
df2.groupBy("possibly_sensitive").count().show()
endTime = int(time.time())
aggregationTime = endTime - startTime

print("############################################################################")
print("Duration of each operation in seconds:")
print("  Read (json/bz2):       {}".format(readBz2Time))
print("  Write (parquet):       {}".format(writeParquetTime))
print("  Read (parquet):        {}".format(readParquetTime))
print("  Aggregation (parquet): {}".format(aggregationTime ))
print("############################################################################")
