#!/bin/bash

# Generates a dataset to be used with logistic regression.

NUM_ROWS="100000"
NUM_COLS="600"

counter=1
while [ $counter -le $NUM_ROWS ]
do
  # Write dependent variable either 0 or 1
  RANGE=2
  number=$RANDOM
  let "number %= $RANGE"
  echo -n "${number}"

  # Write independent variables
  awk -v n=$NUM_COLS -v seed="$RANDOM" 'BEGIN { srand(seed); for (i=0; i<n; ++i) printf(",%.4f", rand()) }'
  echo
  counter=$(( $counter + 1 ))

done
