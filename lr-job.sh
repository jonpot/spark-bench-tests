#!/bin/bash
#SBATCH --job-name=jonpot-spark
#SBATCH --account=support
#SBATCH --partition=standard
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=4
#SBATCH --mem=4g
#SBATCH --time=00:30:00
#SBATCH --mail-type=NONE

module load spark/2.4.5 python/3.7.4 numpy

../spark-on-great-lakes/spark-start

SPARK_MASTER_URL=$(cat ./master.txt)

echo "Spark cluster is running at ${SPARK_MASTER_URL}"
sleep infinity
