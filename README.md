# Spark Bench Tests

These tests can be used with the [spark-bench](https://github.com/CODAIT/spark-bench) tool on ThunderX and Great Lakes. The tool requires Spark v2.4.7 or greater.

## Usage

```bash
# From a login node, clone this repo and cd to the repo

# Clean up output of any previous runs
hdfs dfs -rm -f -r -skipTrash /user/jonpot/csv-vs-parquet/           # On ThunderX
rm -rf /scratch/support_root/support/jonpot/csv-vs-parquet/          # On Great Lakes
rm -rf /nfs/turbo/arcts-data-hadoop-stage/jonpot/csv-vs-parquet/     # On Great Lakes

# On Great Lakes, start a slurm job that starts a standalone spark cluster.
# There are examples in https://bitbucket.org/jonpot/spark-on-great-lakes.
# Get the master node URL, ssh there, and add modules.
ssh <SPARK_MASTER_NODE>
module add spark/2.4.5 python/3.7.4
cd ./spark-bench-tests

# On Thunderx, use Spark 2.4.7.
source /sw/dsi/profile.d/spark-2.4.7-hadoop-2.8.1.sh

# Edit the spark bench test conf file and add the master node URL.
vi ./<TEST>.conf

# Run the benchmark.
<SPARK_BENCH_HOME>/bin/spark-bench.sh ./<TEST>.conf
```

## Benchmark ThunderX and Great Lakes

On ThunderX, the smallest memory allocation is 5 GB per executor, so the tests below use this memory configuration.

### Testing CSV and Parquet File I/O and Spark SQL Query Performance

The `csv-vs-parquet` test consists of two stages. During the first stage, the test generates 2 billion rows by 24 columns of random floating point data. It then writes the data to disk in csv format. The test then reads the data back in from csv and writes the data to disk in parquet format. During the second stage, the test reads in the data from csv and does 2 sql select operations; it then reads the data from parquet and does the same 2 sql select operations. The test can be configured to loop through the second stage multiple times.

The duration of the dataset generation was negligible on both systems (below 1 second). The default configuration always created a dataset consisting of 2 partitions regardless of how many executors or cores were available. As a result of only having 2 partitions, writing the dataset to CSV files took much longer than I anticipated on both systems.

During the write to CSV files, Spark created 1 task per partition resulting in only 2 tasks and 2 large CSV files with the default configuration. This limited the opportunities for parallel operations leaving wasted, idle executors. To optimize this operation, I modified the test to specify 100 partitions of the dataset. This greatly accelerated writing the dataset to CSV files on both systems by allowing parallel operations which utilized all executors.

When reading the CSV files back in from disk, Spark divided the total file size by 128 MB to determine the number of data partitions. This resulted in hundreds or thousands of partitions. Spark generated a tasks to read in each parition and assigned the tasks to executors and the work was done in parallel.

When Spark wrote the dataset to parquet, it generated hundreds or thousands of small partition files of about 51 MB each. Compression was utilized to reduce the 128 MB partition down to about 50 MB on disk. This step also benefited from parallel processing similar to the write to CSV.

Reading from parquet was extremely fast on ThunderX and Great Lakes compared to writing to parquet, or operating on CSV files. Terabytes of parquet data could be ingested on the order of seconds. This is expected as slower writes and extremely fast read times are characteristic of the format.

The results below are from a benchmark job with a 880 GB dataset. The job on ThunderX used 200 executors each with 1 core and 5 GB of memory (1,000 GB total memory). On ThunderX, files were written and read from HDFS. The job on Great Lakes used 30 slurm compute nodes with 200 executors with 1 core and 5 GB of memory each (1,000 GB total memory). Files were written to a turbo directory (`/nfs/turbo/arcts-data-hadoop-stage/`). Results are in seconds.

```
<<<< ThunderX >>>>
----------------------------------------------------------------
| File Type                |   Write   |    Read   |   Query   |
| ------------------------ | --------: | --------: | --------: |
| CSV Dataset              |   888.492 |   774.466 |     0.189 |
| Parquet Dataset          |  1069.541 |     5.912 |     0.058 |
----------------------------------------------------------------

<<<< Great Lakes >>>>
----------------------------------------------------------------
| File Type                |   Write   |    Read   |   Query   |
| ------------------------ | --------: | --------: | --------: |
| CSV Dataset              |   307.471 |   219.834 |     0.040 |
| Parquet Dataset          |   380.251 |     0.658 |     0.014 |
----------------------------------------------------------------
```

### Testing Logistic Regression Performance

Spark on Great Lakes performed better than ThunderX on the spark-bench `lr-bml` logistic regression test. The script `lr-data-gen.sh` generated a synthetic dataset in csv format with 100,000 rows with 600 features. The first 1,000 rows of the dataset were used for training the logistic regression model. On each system, the benchmark was performed with 4 executors with each executor using 1 cpu core and 1 GB of memory. The results of benchmark tests are below.

```
------------------------------------------------
| System               |   Train   |    Test   |
| -------------------- | --------: | --------: |
| ThunderX             |    35.580 |    36.545 |
| Great Lakes          |    16.756 |     6.174 |
------------------------------------------------
```

## Feature Comparison of Great Lakes and ThunderX

I tested code in Python and Scala that utilized Spark v2.4.5 on Great Lakes. I accessed the Spark API interactively using `pyspark` and `spark-shell` and ran standalone apps written in Python and Scala. The Spark features available on ThunderX were also available on Great Lakes.

## Summary

Spark on Great Lakes performed better than ThunderX in all tests. File I/O on Great Lakes was at least 2x faster than ThunderX for read and write regardless of file format. Reading parquet data was particularly interesting where Great Lakes performed nearly 9x faster than ThunderX.

The ThunderX platform runs older hardware than Great Lakes, so these results are not entirely unexpected. However, given that ThunderX utilizes the HDFS filesystem, I was surprised to see data read times on ThunderX significantly slower than Great Lakes with Turbo storage. My assumption is that the network bandwidth between ThunderX nodes at 4 Gbps and their internal disk I/O speeds were the bottleneck. (see footnote 1)

Great Lakes also outperformed ThunderX on the compute intensive Spark SQL query test and Logistic Regression test. The systems vary greatly in age so these results are not surprising.

These results indicate that if researchers who currently use Spark on ThunderX were to move to Spark on Great Lakes, they would likely experience a performance increase and have similar features available for their research.

On the other hand, this approach to running Spark on Great Lakes requires each user to launch a short-lived, standalone spark cluster. This would be more complicated for users than the experience they have today with Spark on ThunderX underpinned by Yarn. Users would have to make many more job architecture decisions about their job requirements than they currently do - such as the number of slurm compute nodes, memory and cpu resources per slurm node, few slurm nodes with many executors each vs. many slurm nodes with few executors each, etc… There is ample opportunity for users to inadvertently create inefficient jobs. As the clusters would be ephemeral, support may be more difficult than today as ARC staff attempt to locate logs after jobs terminate, or try to recreate a user's error when each standalone spark cluster is a unique.

Providing access on Great Lakes to the Spark Web UI (Spark UI) through a local web browser or access to Jupyter, has not yet been solved. Accessing these services will likely not be as user-friendly as on ThunderX where the Spark UI and Jupyter services are currently exposed to any host on the UM network.

### Footnote

1. Consider the test above where data was written to multiple 9 GB CSV files. On write, HDFS breaks these large files into 128 MB blocks. Each small block is written to 3 different ThunderX nodes. If a given executor needs to ingest one of the 9 GB files, it will have to fetch the blocks from many other nodes and piece them together. Occasionally, the executor will get lucky with a local block, but with about 30 HDFS nodes and only 3x replication of the HDFS blocks, around 90% of the blocks would come from other nodes. The fetch across the network combined with a a slower internal disk I/O of the ThunderX nodes is probably the driver of the slower read times.


